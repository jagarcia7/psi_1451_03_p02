dropdb -U alumnodb -h localhost psi
createdb -U alumnodb -h localhost psi
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
python populate_rango.py
python manage.py runserver